<?php

class PublicFilesUrlStreamWrapper extends DrupalPublicStreamWrapper {

  /**
   * Override: getExternalUrl().
   */
  public function getExternalUrl() {
    $url = parent::getExternalUrl();


    // If configured, point static file locations to
    // the alternate domain.
    if (!$host = variable_get('public_files_url', FALSE)) {
      return $url;
    }

    // Break the url down into its components.
    $components = parse_url($url) + array(
      // Ensure components always has a path key.
      'path' => '/',
    );

    // Hard set the host domain.
    $components['host'] = $host;

    if (!isset($components['scheme'])) {
      global $is_https;
      $components['scheme'] = $is_https ? 'https' : 'http';
    }

    // Recompile the URL.
    $url = '';
    if (isset($components['scheme'], $components['host'])) {
      $url = $components['scheme'] . '://' . $components['host'];
    }

    // Add the path back.
    $url .= $components['path'];

    if (!empty($components['query'])) {
      $url .= '?' . $components['query'];
    }

    if (!empty($components['fragment'])) {
      $url .= '#' . $components['fragment'];
    }
    
    return $url;
  }

}
